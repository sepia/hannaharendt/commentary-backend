# Commentary Backend

This app is currently divided in two main parts:

- [RestXQ module](src/modules/api.xqm)
- [XQuery library](src/content/lib.xqm)

## API (draft, pre-release, unstable)

The RestXQ module registers the following API entrypoint for node operations:

`/commentaries/<commentaryId>/comment/<itemId>/note/<noteNr>`

Supported operations: `GET`, `POST`

Query Parameter(s): `node` with currently supported values `corresp`, `lang`, `rend`, [TBD: from, to, n, ...]

The RestXQ module registers the following additional API endpoints:

- `GET /languages/<editionId>`
- `GET /languages`
- `GET /files`
- `GET /items/<commentaryId>/<editionId>`
- `GET /items/<commentaryId>`

## Package Dependencies

[functx 1.0.1](http://exist-db.org/exist/apps/public-repo/packages/functx.html)
