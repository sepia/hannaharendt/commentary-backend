xquery version "3.1";

(:~ CRUD API sorted by
 :   1. Endpoint hierarchy
 :   2. CRUD operation
 :
 :
 : @author Stefan Hynek
 :)
module namespace api="http://sepia.io/commentary-backend/api";

(: once this is resgistered as a library, remove the path :)
import module namespace main="http://sepia.io/commentary-backend/lib" at "../content/lib.xqm";


declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace rest="http://exquery.org/ns/restxq";


(: this may be replaced by a query-parameter or any other implementation :)
declare variable $api:default-data-collection := "/db/data";


(: TODO: rename path "comment" to "comments" :)


(:~ Retrieve all commentaries.
 :)
declare
    %rest:GET
    %rest:path("/commentaries")
    %rest:query-param("select", "{$select}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:retrieve-commentaries($select as xs:string*)
{
    main:retrieve-commentaries($api:default-data-collection, $select)
};

(:~ Retrieve all editions.
 : Supported values for select query parameter:
 : - id: returns document ids
 : - language: returns a merged map of used languages
 : Default return is an array of all edition documents.
 :)
declare
    %rest:GET
    %rest:path("/editions")
    %rest:query-param("select", "{$select}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:retrieve-editions($select as xs:string*)
{
    main:retrieve-editions($api:default-data-collection, $select)
};


(:~ Retrieve a certain edition.
 : Supported values for select query parameter:
 : - id: returns comment ids
 : - language: returns a map of used languages
 : Default return is the edition document.
 :)
declare
    %rest:GET
    %rest:path("/editions/{$editionId}")
    %rest:query-param("select", "{$select}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:retrieve-edition($editionId, $select as xs:string*)
{
    main:retrieve-edition($api:default-data-collection, $editionId, $select)
};

(:~
 : Retrieve all comments from a commentary file.
 : query parameter "select" to select object attribute: either "id" or "notes"
 :)
declare
    %rest:GET
    %rest:path("/commentaries/{$commentaryId}")
    %rest:query-param("select", "{$select}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:retrieve-comments($commentaryId as xs:string, $select as xs:string*)
{
    (: TODO: add query parameter to retrieve IDs only :)
    main:retrieve-comments($api:default-data-collection, $commentaryId, $select)
};

(:~
 : Create a comment.
 : An empty body creates a comment with a random ID; otherwise a string is expected to be send with in the message body.
 :)
declare
    %rest:POST("{$body}")
    %rest:path("/commentaries/{$commentaryId}/comments")
    %rest:header-param("Content-Type", "{$content-type}")
    %rest:consumes("application/json", "text/*")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:create-comment($commentaryId as xs:string, $body, $content-type as xs:string*)
    as element(rest:response)
{
    let $data :=
        if (not($content-type => fn:starts-with("text")))
        then util:base64-decode($body)
        else $body

    return
        main:create-comment($api:default-data-collection, $commentaryId, $data)
};


(:~
 : Retrieve a comment.
 :)
declare
    %rest:GET
    %rest:path("/commentaries/{$commentaryId}/comments/{$commentId}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:retrieve-comment($commentaryId as xs:string, $commentId as xs:string)
{
    main:retrieve-comment($api:default-data-collection, $commentaryId, $commentId)
};




(:~
 : Create a Note.
 :)
declare
    %rest:POST
    %rest:path("/commentaries/{$commentaryId}/comments/{$commentId}/notes")
function api:create-note($commentaryId as xs:string, $commentId as xs:string)
    as element(rest:response)
{
    main:create-note($api:default-data-collection, $commentaryId, $commentId)
};



(:~
 : CREATE a single node with/without content.
 : Attention: %rest:consumes cannot be used with empty body!
 :)
declare
    %rest:POST("{$body}")
    %rest:path("/commentaries/{$commentaryId}/comments/{$itemId}/notes/{$noteNr}")
    %rest:query-param("node", "{$node}")
    %rest:header-param("Content-Type", "{$content-type}")
(:    %rest:consumes("application/json", "text/*"):)
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:create-node($commentaryId as xs:string, $itemId as xs:string, $noteNr as xs:integer, $body as item()*, $node as xs:string*, $content-type as xs:string*)
{
    let $data :=
        if ($content-type)
        then
            if (not($content-type => fn:starts-with("text")))
            then util:base64-decode($body)
            else $body
        else
            ()

    return
        main:create-node($api:default-data-collection, $commentaryId, $itemId, $noteNr, $node, $data)
};

(:~
 : RETRIEVE the value of a single node from a note, including the parent node, the note itself.
 :)
declare
    %rest:GET
    %rest:path("/commentaries/{$commentaryId}/comments/{$itemId}/notes/{$noteNr}")
    %rest:query-param("node", "{$node}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:retrieve-node($commentaryId as xs:string, $itemId as xs:string, $noteNr as xs:integer, $node as xs:string*)
{
    main:retrieve-node($api:default-data-collection, $commentaryId, $itemId, $noteNr, $node)
};

(:~
 : UPDATE the value of a single node.
 :)
declare
    %rest:PUT("{$body}")
    %rest:path("/commentaries/{$commentaryId}/comments/{$itemId}/notes/{$noteNr}")
    %rest:query-param("node", "{$node}")
    %rest:header-param("Content-Type", "{$content-type}")
    %rest:consumes("application/json", "text/*")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:update-node($commentaryId as xs:string, $itemId as xs:string, $noteNr as xs:integer, $body, $node as xs:string*, $content-type as xs:string*)
{
    let $data :=
        if (not($content-type => fn:starts-with("text")))
        then util:base64-decode($body)
        else $body

    return
(: was: update-node       :)
        main:update-node($api:default-data-collection, $commentaryId, $itemId, $noteNr, $node, $data)
};

(:~
 : DESTROY a single node from a note, including (=OR) the parent node, the note itself.
 :)
declare
    %rest:DELETE
    %rest:path("/commentaries/{$commentaryId}/comments/{$itemId}/notes/{$noteNr}")
    %rest:query-param("node", "{$node}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function api:destroy-node($commentaryId as xs:string, $itemId as xs:string, $noteNr as xs:integer, $node as xs:string*)
{
    main:destroy-node($api:default-data-collection, $commentaryId, $itemId, $noteNr, $node)
};
