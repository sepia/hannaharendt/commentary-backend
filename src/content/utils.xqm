xquery version "3.1";

(:~
 : This library provides helper functions of all kinds.
 :
 : @author Stefan Hynek
 :)
module namespace cbu="http://sepia.io/commentary-backend/utils";

declare namespace http="http://expath.org/ns/http-client";
declare namespace rest="http://exquery.org/ns/restxq";

(:~ HTTP Status codes and their respective messages. :)
declare variable $cbu:status-messages as map(xs:integer, xs:string) :=
    map {
        200 : "OK",
        201 : "Created",
        202 : "Accepted",
        404 : "Not Found",
        409 : "Conflict"
    };


(:~ Build a sequence of a rest:response and a message body.
 :
 : @param $status-code HTTP status code
 : @param $headers Map of additional headers
 : @param $body Message body
 : @return Sequence of rest:response and message body sequence items
 :)
declare function cbu:rest-response-builder($status-code as xs:integer, $headers as map(xs:string, xs:string), $body as item()*)
    as item()+
{
    element rest:response {
            element http:response {
                attribute status { xs:string($status-code) },
                attribute message { $cbu:status-messages($status-code) },
                for $name in map:keys($headers)
                return
                    element http:header {
                        attribute name {$name},
                        attribute value { $headers($name) }
                    }
            }
        },
        $body
};

(:~ Build a rest:response object
 :
 : @param $status-code HTTP status code
 : @param $headers Map of additional headers
 : @return REST response object
 :)
declare function cbu:rest-response-builder($status-code as xs:integer, $headers as map(xs:string, xs:string))
    as element(rest:response)
{
    cbu:rest-response-builder($status-code, $headers, ())
};

(:~ Update a comment node value.
 :
 : Work around the fact that updating a comment node is unsupported in exist-db update facilities.
 : returns the empty-sequence as all (async) update operations do.
 :
 : @author Mathias Goebel
 : @author Stefan Hynek
 :
 : @param $node the comment node
 : @param $value update value
 :)
declare function cbu:update-value-comment-node($node as comment(), $value as xs:string)
    as empty-sequence()
{
    (: With the following two update statements, it could be that $node is deleted before the new value could be inserted.
     : This should be prevented by using this implementation specific pragma,
     : see https://exist-db.org/exist/apps/doc/xquery#pragmas
     :)
    (# exist:batch-transaction #) {
        update insert comment { $value } following $node,
        update delete $node
    }
};
