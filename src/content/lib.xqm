xquery version "3.1";

(:~
: This library provides the means to return values for the REST-API module.
:
: @author Stefan Hynek
:)
module namespace main="http://sepia.io/commentary-backend/lib";

import module namespace cbu="http://sepia.io/commentary-backend/utils" at "utils.xqm";
import module namespace functx = "http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";


(: CREATE OPERATIONS :)

(:~ Create a new comment.
 :
 :  @return A rest:response with a "location" header that points to the new resource
 :)
declare function main:create-comment($collection, $commentaryId, $data)
    as element(rest:response)
{
    let $doc := main:get-commentary-doc($collection, $commentaryId)
    let $commentId :=
        if ($data)
        then
            $data
        else
            fn:string-join(("_", util:uuid() => fn:substring-before("-")), "")

    let $status :=
        if (main:get-item($collection, $commentaryId, $commentId))
            (: if the comment to be created already exists, send HTTP 409 and don't update :)
            then 409
            (: if the comment does not exist, return HTTP 202 and insert it :)
            else (
                202,
                update insert element tei:item {attribute xml:id {$commentId}} into $doc
            )
    let $headers := map {
        "location" : fn:concat("/commentaries/", $commentaryId, "/comments/", $commentId)
    }

    return
        cbu:rest-response-builder($status, $headers)
};

(: RETRIEVE OPERATIONS :)

(: ENTRYPOINTS FOR CRUD OPERATIONS ON NODES :)

declare function main:retrieve-commentaries($collection as xs:string+, $select as xs:string*){
    let $commentaryIds := main:get-commentaries($collection)

    let $status :=
        if (fn:count($commentaryIds) > 0)
        then 200
        else 404

    let $returns :=
    if ($status = 200)
    then
        switch ($select)
        case "id" return
            $commentaryIds
        default return
            for $commentaryId in $commentaryIds
            return main:get-commentary-doc($collection, $commentaryId)
    else ()

    return
        cbu:rest-response-builder($status, map {}, $returns)
};

(:~ Retrieve all editions.
 :
 :)
declare function main:retrieve-editions($collection as xs:string+, $select as xs:string*){
    let $editionIds := main:get-editions($collection)

    let $status :=
        if (fn:count($editionIds) > 0)
        then 200
        else 404

    let $returns :=
    if ($status = 200)
    then
        switch ($select)
        case "id" return
            $editionIds
        case "language" return
            map:merge(
                for $editionId in $editionIds
                return main:get-languages($collection, $editionId)
            )
        default return
            for $editionId in $editionIds
            return main:get-edition-doc($collection, $editionId)
    else ()

    return
        cbu:rest-response-builder($status, map {}, $returns)
};

(:~ Retrieve a certain edition.
 :)
declare function main:retrieve-edition($collection  as xs:string+, $editionId as xs:string+, $select as xs:string*){
    let $doc := main:get-edition-doc($collection, $editionId)
    let $status :=
        if ($doc)
        then 200
        else 404

    let $returns :=
    if ($status = 200)
    then
        switch ($select)
        case "id" return
            main:get-quote-ids-from-edition($collection, $editionId)
        case "language" return
            main:get-languages($collection, $editionId)
        default return
            $doc
    else ()

    return
        cbu:rest-response-builder($status, map {}, $returns)
};

declare function main:retrieve-comments($collection as xs:string+, $commentaryId as xs:string, $select as xs:string*)
{
    let $doc := main:get-commentary-doc($collection, $commentaryId)
    let $items := $doc/tei:item
    (: NOW: set status 200 if $items, else 404; return object or attribute according to the value of the query parameter "select" :)
    let $status :=
        if ($items)
        then 200
        else 404

    let $comments :=
    if ($status = 200)
    then
        switch ($select)
        case "id" return
            for $item in $items
            return
                $item/string(@xml:id)
        case "filename" return
            util:document-name($doc)
        default return
            for $item in $items
            return
                map {
                    "id" : $item/string(@xml:id),
                    "notes" : array {
                        main:get-notes($item)
                        => main:create-note-maps()
                    }
                }
    else ()

    return
        cbu:rest-response-builder($status, map {}, $comments)
};

(:~ Retrieve a comment.
 :
 :)
declare function main:retrieve-comment($collection, $commentaryId, $commentId)
{
    let $comment := main:get-comment($collection, $commentaryId, $commentId)

    let $status :=
        if (map:size($comment) > 0)
        then 200
        else 404

    return
        cbu:rest-response-builder($status, map {}, $comment)
};



(:~ Retrieve a Note.
 :
 :)
declare function main:retrieve-note($collection, $commentaryId, $commentId, $noteNr)
    as item()+
{
    let $note := main:get-note($collection, $commentaryId, $commentId, $noteNr)

    let $status :=
        if ($note)
            then
                200
            else
                404

    return
        cbu:rest-response-builder($status, map {}, $comment)
};

declare function main:create-node($collection, $commentaryId as xs:string, $itemId as xs:string, $noteNr as xs:integer, $node as xs:string*, $data)
{
    let $note := main:get-note($collection, $commentaryId, $itemId, $noteNr)

    return
    switch ($node)
        case "lang"
            return update insert attribute xml:lang { $data } into $note
        case "rend"
            return update insert attribute rend {"show"} into $note
        case "comm"
            return
                let $seq :=
                    for $x in parse-json($data)?* return parse-xml-fragment($x)
                return update insert $seq into $note
        case "internal"
            return
            if ($note/tei:bibl)
            then
                update insert comment { $data } into $note/tei:bibl
            else
                update insert element tei:bibl { comment { $data } } into $note
        case "corresp"
            return
            if ($note/tei:bibl)
            then
                update insert attribute corresp {$data} into $note/tei:bibl
            else
                update insert element tei:bibl { attribute corresp {$data} } into $note
        case "reference"
        return
            if ($note/tei:bibl)
            then
                update insert text { $data } into $note/tei:bibl
            else
                update insert element tei:bibl { text { $data } } into $note
        case "n"
            return update insert attribute n { $data } into $note/tei:bibl/tei:biblScope
        case "from"
            return update insert attribute from { $data } into $note/tei:bibl/tei:biblScope
        case "scope"
            return update insert text { $data } into $note/tei:bibl/tei:biblScope
        case "to"
            return update insert attribute to { $data } into $note/tei:bibl/tei:biblScope
        default (: return HTTP 40X: :)
            return ()
};

(:  REFACTORED VERSION OF main:update-node :)
declare function main:update-node($collection, $commentaryId as xs:string, $itemId as xs:string, $noteNr as xs:integer, $node as xs:string*, $data)
{
    let $note := main:get-note($collection, $commentaryId, $itemId, $noteNr)

    return
    switch ($node)
        case "internal"
            return cbu:update-value-comment-node($note/tei:bibl/comment(), $data)
        case "n"
            return update value $note/tei:bibl/tei:biblScope/@n with $data
        case "comm"
            return
                (
                    update delete $note/tei:p,
                    for $x in parse-json($data)?*
                        return update insert element tei:p {parse-xml-fragment($x)} into $note
                )
        case "corresp"
            return update value $note/tei:bibl/@corresp with $data
        case "from"
            return update value $note/tei:bibl/tei:biblScope/@from with $data
        case "lang"
            return update value $note/@xml:lang with $data
        case "reference"
            return update value $note/tei:bibl/text() with $data
        (: the "rend" case is kept here for structural unity although it will usually not be updated in this use-case.
         : it can only be created or deleted. this was a design choice in the commentary schema, see there (does not exist...).
         :)
        case "rend"
            return update value $note/@rend with $data
        case "scope"
            return update value $note/tei:bibl/tei:biblScope/text() with $data
        case "to"
            return update value $note/tei:bibl/tei:biblScope/@to with $data
        default
            return $node
};


declare function main:update-node-DEPRECATED($collection, $commentaryId as xs:string, $itemId as xs:string, $noteNr as xs:integer, $node as xs:string*, $data)
{
    let $note := main:get-note($collection, $commentaryId, $itemId, $noteNr)

    return
    switch ($node)
        case "n"
            return main:update-n($note, $data)
        case "corresp"
            return main:update-corresp($note, $data)
        case "from"
            return main:update-from($note, $data)
        case "lang"
            return main:update-lang($note, $data)
        case "reference"
            return main:update-reference($note, $data)
        case "rend"
            return main:update-rend($note, $data)
        case "scope"
            return main:update-scope($note, $data)
        case "to"
            return main:update-to($note, $data)
        default
            return $node
};

declare function main:retrieve-node($collection, $commentaryId, $itemId, $noteNr as xs:integer, $node)
{
    let $note := main:get-note($collection, $commentaryId, $itemId, $noteNr)

    return
        if ($note)
        then
            switch ($node)
            case "internal"
                return $note/tei:bibl/string(comment())
            case "n"
                return $note/tei:bibl/tei:biblScope/string(@n)
            case "comm"
                return array{ for $p in $note/tei:p return array {$p/node()}}
            case "corresp"
                return $note/tei:bibl/string(@corresp) => functx:substring-after-if-contains("#")
            case "from"
                return $note/tei:bibl/tei:biblScope/string(@from)
            case "lang"
                return $note/string(@xml:lang)
            case "reference"
                return $note/tei:bibl/text()
            case "rend"
                return fn:boolean($note/string(@rend))
            case "scope"
                return $note/tei:bibl/tei:biblScope/text()
            case "to"
                return $note/tei:bibl/tei:biblScope/string(@to)
            default
                return $note
        else
            ()

};


declare function main:destroy-node($collection, $commentaryId, $itemId, $noteNr as xs:integer, $node)
{
    let $note := main:get-note($collection, $commentaryId, $itemId, $noteNr)

    return
        if ($note)
        then
            switch ($node)
            case "internal"
                return update delete $note/tei:bibl/comment()
            case "n"
                return update delete $note/tei:bibl/tei:biblScope/@n
            case "comm"
                return  update delete $note/tei:p
            case "corresp"
                return update delete $note/tei:bibl/tei:biblScope/@corresp
            case "from"
                return update delete $note/tei:bibl/tei:biblScope/@from
            (: Once created, "lang" cannot be deleted. :)
            (: case "lang":)
            (:   return update delete $note/@xml:lang:)
            case "reference"
                return update delete $note/tei:bibl/text()
            case "rend"
                return update delete $note/@rend
            case "scope"
                return update delete $note/tei:bibl/tei:biblScope/text()
            case "to"
                return update delete $note/tei:bibl/tei:biblScope/@to
            default
                return update delete $note
        else
            ()

};

(: CREATE OPERATIONS :)

(: accept job and return location header
 : 1. get number of notes
 : 2. create note (async)
 : 3. return ACCEPT
 :
 : this function only creates the "note" itself; for note-nodes, see create-node
 : BUT: some nodes are parents of the to-be-created nodes that are created here
 : for convenience. this is a problem of the schema design.
 :)
declare function main:create-note($collection, $commentaryId, $commentId)
{
    let $comment := main:get-item($collection, $commentaryId, $commentId)

    (: noteNr is the number of existing Notes +1 but API is exspected to return Ids starting with 0 (while xq starts counting from 1) :)
    let $noteNr := fn:count($comment/tei:note)

    let $status :=
        if ($comment)
        then (
            202,
            update insert element
                tei:note {
                    element tei:bibl {
                        element tei:biblScope {
                            attribute unit {"page"}
                        }
                    }
                }
            into $comment
        )
        else
            404


    let $headers := map {
        "location" : fn:concat("/commentaries/", $commentaryId, "/comments/", $commentId, "/notes/", $noteNr)
    }

    return
        cbu:rest-response-builder($status, $headers)

};

declare function main:create-bibl() {()};

declare function main:create-biblScope() {()};


(: NODE RETRIEVE OPERATIONS :)

declare function main:get-n($note as element(tei:note))
    as xs:string?
{
    $note/tei:bibl/tei:biblScope/string(@n)
};

declare function main:get-from($note as element(tei:note))
    as xs:string?
{
    $note/tei:bibl/tei:biblScope/string(@from)
};

declare function main:get-reference($note as element(tei:note))
    as xs:string?
{
    $note/tei:bibl/text()
};

declare function main:get-rend($note as element(tei:note))
    as xs:boolean?
{
    fn:boolean($note/string(@rend))
};

declare function main:get-scope($note as element(tei:note))
    as xs:string?
{
    $note/tei:bibl/tei:biblScope/text()
};

declare function main:get-to($note as element(tei:note))
    as xs:string?
{
    $note/tei:bibl/tei:biblScope/string(@to)
};

declare function main:get-corresp($note as element(tei:note))
    as xs:string?
{
    $note/tei:bibl/string(@corresp) => functx:substring-after-if-contains("#")
};

declare function main:get-lang($note as element(tei:note))
    as xs:string?
{
    $note/string(@xml:lang)
};


(: NODE UPDATE OPERATIONS :)
(: TODO: REFACTOR to create, update and delete/destroy :)

declare function main:update-n($note as element(tei:note), $body)
{
    if ($body)
    then
        if ($note/tei:bibl/tei:biblScope/string(@n))
        then
            update replace $note/tei:bibl/tei:biblScope/@n with attribute n {$body}
        else
            update insert attribute n { $body } into $note/tei:bibl/tei:biblScope
    else
        update delete $note/tei:bibl/tei:biblScope/@n

};

declare function main:update-corresp($note, $body)
{
    if ($body)
    then
        if ($note/tei:bibl/@corresp)
        then
            update replace $note/tei:bibl/@corresp with attribute corresp {$body}
        else
            update insert element tei:bibl { attribute corresp {$body} } into $note
    else
        update delete $note/tei:bibl
};

declare function main:update-from($note as element(tei:note), $body)
{

    if ($body)
    then
        if ($note/tei:bibl/tei:biblScope/string(@from))
        then
            update replace $note/tei:bibl/tei:biblScope/@from with attribute from {$body}
        else
            update insert attribute from { $body } into $note/tei:bibl/tei:biblScope
    else
        update delete $note/tei:bibl/tei:biblScope/@from

};

declare function main:update-lang($note, $body)
{
    if ($note/@xml:lang)
    then
        update value $note/@xml:lang with $body
    else
        update insert attribute xml:lang {$body} into $note
};

declare function main:update-reference($note, $body)
{
        if ($body)
        then
            update insert $body into $note/tei:bibl
        else
            update delete $note/tei:bibl/text()
};


declare function main:update-rend($note, $body)
{
        if ($body)
        then
            update insert attribute rend {"show"} into $note
        else
            update delete $note/@rend
};

declare function main:update-scope($note, $body)
{
        if ($body)
        then
            update insert $body into $note/tei:bibl/tei:biblScope
        else
            update delete $note/tei:bibl/tei:biblScope/text()
};

declare function main:update-to($note as element(tei:note), $body)
{
    if ($body)
    then
        if ($note/tei:bibl/tei:biblScope/string(@to))
        then
            update replace $note/tei:bibl/tei:biblScope/@to with attribute to {$body}
        else
            update insert attribute to { $body } into $note/tei:bibl/tei:biblScope
    else
        update delete $note/tei:bibl/tei:biblScope/@to

};


(: HIGHER ORDER ELEMENTS :)

(:~
 : Get a note element from a commentary item (aka comment).
 :
 : @author Stefan Hynek
 :
 :)
declare function main:get-note($collection, $commentaryId, $itemId, $noteNr as xs:integer)
    as element(tei:note)?
{
    (main:get-item($collection, $commentaryId, $itemId)/tei:note)[$noteNr + 1]
};

(:~
 : Get an item element from a commentary.
 :
 : @author Stefan Hynek
 :
 : @param $collection Collection(s) to be searched for the commentaryId
 : @param $commentaryId The id of the commentary to query item elements from
 : @param $id The item id
 : @return The element(tei:item) with the given id.
 :)
declare function main:get-item($collection as xs:string+, $commentaryId as xs:string, $itemId as xs:string)
    as element(tei:item)?
{
    main:get-commentary-doc($collection, $commentaryId)//tei:item[@xml:id=$itemId]
};

(:~
 : Get
 :
 : @author Stefan Hynek
 :
 : @param $collection Collection(s) to be searched for the commentaryId
 : @param $commentaryId Id of the commentary to be returned
 : @return element(tei:list)
 :)
declare function main:get-commentary-doc($collection as xs:string+, $commentaryId as xs:string)
    as element(tei:list)?
{
    collection($collection)//tei:list[@xml:id=$commentaryId]
};









(: LEGACY and to-be-refactored FUNCTIONS ... :)

(:~
 : Get languages from an edition.
 :
 : @author Stefan Hynek
 :
 : @param $collection Collection(s) to be searched for the editionId
 : @param $editionId Id of the edition to query languages from
 : @return Map of languages used in the edition
 :)
declare function main:get-languages($collection as xs:string+, $editionId as xs:string)
    as map(xs:string, xs:string)?
{
    map:merge(
        for $lang in main:get-edition-doc($collection, $editionId)/tei:teiHeader/tei:profileDesc/tei:langUsage/*
        return map {$lang/string(@ident) : $lang/string()}
    )
};

(:~
 : Get languages from a collection/collections of documents.
 :
 : @author Stefan Hynek
 :
 : @param $collection Collection(s) to be searched for the editionId
 : @return Map of languages used in the edition(s)
 :)
declare function main:get-languages($collection as xs:string+)
    as map(xs:string, xs:string)?
{
    map:merge(
        for $lang in collection($collection)//tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/*
        return map {$lang/string(@ident) : $lang/string()}
    )
};

(:~
 : Get filenames without extension from a collection/collections of documents.
 :
 : @author Stefan Hynek
 :
 : @param $collection Collection(s) to be searched for the editionId
 : @return Map of languages used in the edition
 :)
declare function main:get-files($collection as xs:string+)
    as xs:string*
{
    for $resource in xmldb:get-child-resources($collection)
    return functx:substring-before-last($resource, ".")
};


(:~
 : Get quotation ids from a collection of documents.
 :
 : @author Stefan Hynek
 :
 : @param $collection Collection(s) to be searched for the editionId
 : @param $editionId Id of the edition to query quote ids from
 : @return Array of Ids
 :)
declare function main:get-quote-ids-from-edition($collection as xs:string+, $editionId as xs:string)
    as array (xs:string)
{
(: why am i using an array here? sequences are rendered to arrays on serialization...
 : BUT if the sequence comprises only one single item, it is not converted to an array
 :)
    array {for $quote in main:get-edition-doc($collection, $editionId)//tei:quote return $quote/@xml:id/string()}
};



(:~
 : Get notes from an item.
 :
 : @author Stefan Hynek
 :
 : @param $item The item element to return the notes from
 : @return Sequence of one or more element(tei:note)
 :)
declare function main:get-notes($item as element(tei:item))
    as element(tei:note)*
{
    $item/tei:note
};

(:~
 : Get
 :
 : @author Stefan Hynek
 :
 : @param $collection Collection(s) to be searched for the editionId
 : @param $editionId Id of the edition to be returned
 : @return element(tei:TEI)
 :)
declare function main:get-edition-doc($collection as xs:string+, $editionId as xs:string)
    as element(tei:TEI)?
{
    collection($collection)//tei:TEI[@xml:id=$editionId]
};



(:~
 : Get commentary IDs.
 :
 : @author Stefan Hynek
 :
 : @param $collection Collection(s) to be searched
 : @return one or more commentary ids
 :)
declare function main:get-commentaries($collection as xs:string+)
    as xs:string*
{
    collection($collection)//tei:list/string(@xml:id)[. != ""]
};

declare function main:get-editions($collection as xs:string+)
    as xs:string*
{
    collection($collection)//tei:TEI/string(@xml:id)[. != ""]
};


declare function main:create-note-maps($notes as element(tei:note)*)
    as map(*)*
{
    for $note in $notes return main:create-note-map($note)
};

declare function main:create-note-map($note as element(tei:note))
as map(*)
{
    map {
        "lang" : $note/string(@xml:lang),
        "rend" : $note/string(@rend),
        "bibl" : main:create-bibl($note/tei:bibl),
        "comm" : array{ for $p in $note/tei:p return array {$p/node()}}
        }
};

declare function main:create-bibl($bibl as element(tei:bibl)*)
    as map(xs:string, xs:string)
{
    map {
        "internal" : $bibl/string(comment()),
        "corresp" : $bibl/string(@corresp) => functx:substring-after-if-contains("#"),
        "from" : $bibl/tei:biblScope/string(@from),
        "to" : $bibl/tei:biblScope/string(@to),
        "n" : $bibl/tei:biblScope/string(@n),
        "scope" : $bibl/tei:biblScope/string(),
        "reference" : $bibl/string()
    }
};




declare function main:get-commentary-items($collection as xs:string+, $commentaryId as xs:string, $editionId as xs:string)
    as map(*)*
{
    for $quoteId in main:get-quote-ids-from-edition($collection, $editionId)?*
    return
        map {
            "id" : $quoteId,
            "notes" : array {
                main:get-item($collection, $commentaryId, $quoteId)
                => main:get-notes()
                => main:create-note-maps()
            }
    }
};

declare function main:get-commentary-items($collection as xs:string+, $commentaryId as xs:string)
    as map(*)*
{
    for $item in main:get-commentary-doc($collection, $commentaryId)/tei:item(:get all item-ids from commentary:)
    return
        map {
            "id" : $item/string(@xml:id),
            "notes" : array {
                main:get-notes($item)
                => main:create-note-maps()
            }
    }
};

declare function main:get-item-notes($collection as xs:string+, $commentaryId as xs:string, $itemId as xs:string)
{
    main:get-commentary-doc($collection, $commentaryId)/tei:item[@xml:id=$itemId]
    => main:get-notes()
    => main:create-note-maps()

};

(: currently, for testing purposes only; design choice is to update single nodes :)
declare function main:update-commentary-item($collection as xs:string+, $commentaryId as xs:string, $itemId as xs:string, $body)
{
    let $jsonXML := fn:json-to-xml($dataString)
    let $doc := doc("/db/data/III-commentaryDataSechsEssays.xml")

    let $insertData := for $note in $jsonXML/fn:array/*
        return
            element note
                {
                    attribute xml:lang {$note/fn:string[@key="lang"]/string()},
                    if ($note/fn:string[@key="rend"])
                    then attribute rend {$note/fn:string[@key="rend"]/string()}
                    else (),
                    element bibl {
                        attribute corresp {$note/fn:map[@key="bibl"]/fn:string[@key="corresp"]/string()},
                        element biblScope {
                            attribute unit {$note/fn:map[@key="bibl"]/fn:string[@key="unit"]/string()},
                            attribute from {$note/fn:map[@key="bibl"]/fn:string[@key="from"]/string()},
                            attribute to {$note/fn:map[@key="bibl"]/fn:string[@key="to"]/string()},
                            attribute n {$note/fn:map[@key="bibl"]/fn:string[@key="n"]/string()}
                        },
                        $note/fn:map[@key="bibl"]/fn:string[@key="#text"]/string()
                    },
                    element p {$note/fn:string[@key="comm"]/string()}
                }
    return
        util:log("info", $insertData),
        update replace $doc//tei:item[@xml:id="verTradq1"] with $insertData

};

(: HELPER, only called module-internal :)

declare function main:get-comment($collection as xs:string+, $commentaryId as xs:string, $commentId as xs:string)
    as map(*)?
{
    let $comment := main:get-commentary-doc($collection, $commentaryId)/tei:item[@xml:id=$commentId]
    return
    if ($comment)
    then
        map {
                "id" : $comment/string(@xml:id),
                "notes" : array {
                    main:get-notes($comment)
                    => main:create-note-maps()
                }
        }
    else map{}
};
