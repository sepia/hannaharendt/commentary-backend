xquery version "3.1";

declare namespace sm="http://exist-db.org/xquery/securitymanager";

(: the target collection into which the app is deployed :)
declare variable $target external;

if (repo:get-resource("http://sepia.io/commentary-backend", "repo.xml") => exists())
then util:log("info", "Package successfully deployed.")
else util:log("error", "Package not successfully deployed."),
sm:chmod(xs:anyURI($target || "/modules/api.xqm"), "rwsr-xr-x"),
util:log("info", "API module file mode: " || sm:get-permissions(xs:anyURI($target || "/modules/api.xqm"))//@mode/string())